package Assigment1;

public class Fighter {
    private int mBaseHp;
    private int mWp;

    public double getCombatScore()
    {
        return 0.0;
    }

    public Fighter(int BaseHp, int Wp) {
        this.mBaseHp = BaseHp;
        this.mWp = Wp;
    }

    public int getmBaseHp() {
        return mBaseHp;
    }

    public void setmBaseHp(int mBaseHp) {
        this.mBaseHp = mBaseHp;
    }

    public int getmWp() {
        return mWp;
    }

    public void setmWp(int mWp) {
        this.mWp = mWp;
    }
}
