package Assigment1;

public class Knight extends Fighter{

    public Knight(int BaseHp, int Wp) {
        super(BaseHp, Wp);
    }

    @Override
    public double getCombatScore() {
        double baseHP = getmBaseHp();
        if (Utility.Ground == 999) {
            int last = 1;
            int next = 0;
            int sum;
            while(true) {
                sum = next + last;
                next = last;
                last = sum;
                if (sum > 2 * baseHP)
                    return sum;
            }
        }

        if (Utility.isSquare(Utility.Ground))
            return 2 * getmBaseHp();
        else
            return getmWp() == 1 ? getmBaseHp() : getmBaseHp() / 10.0;

    }
}
