package Assigment1;

public class Warrior extends Fighter{
    public Warrior(int BaseHp, int Wp) {
        super(BaseHp, Wp);
    }
    @Override
    public double getCombatScore() {
        if (Utility.isPrime(Utility.Ground))
            return 2 * getmBaseHp();
        else
            return getmWp() == 1 ? getmBaseHp() : getmBaseHp() / 10.0;
    }
}
